import React from 'react';
import {NumberContextConsumer} from './NumberContextProvider'

const NumberCalculator = () => {
    const signs = ['+', '-', '/', '*'];
    const calc = (firstNumber, secondNumber, sign) => {
        if (secondNumber === 0 && sign === '/') {
            return 'Ошибка, деление на 0!'
        }

        return eval(`${firstNumber} ${sign} ${secondNumber}`)
    }
    return (
        <NumberContextConsumer>
            {context => (
                <div>
                    <span>Результат: </span>
                    <br/>
                    <span>{context.firstNumber}</span>
                    <select
                        onChange={context.onSignChange}
                        value={context.sign}
                        style={{marginLeft: '10px', marginRight: '10px'}}
                    >
                        {signs.map((sign, key) => (
                            <option key={key}>{sign}</option>
                        ))}
                    </select>
                    <span>{context.secondNumber}</span>
                    <span style={{marginLeft: '10px', marginRight: '10px'}}> = </span>
                    <span>{calc(context.firstNumber, context.secondNumber, context.sign)}</span>
                </div>
            )}
        </NumberContextConsumer>
    )
}

export default NumberCalculator
