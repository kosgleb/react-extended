import React from 'react';
import {NumberContextConsumer} from './NumberContextProvider'

const NumberSelector = () => {
    const onNumberChange = (context, number, event) => context.onNumberChange(number, parseInt(event.target.value));

    return (
        <NumberContextConsumer>
            {context => (
                <div>
                    <div>
                        <label>Первое число</label>
                        <input value={context.firstNumber} onChange={onNumberChange.bind(this, context, 'firstNumber')} type="number"/>
                    </div>
                    <div>
                        <label>Второе число</label>
                        <input value={context.secondNumber} onChange={onNumberChange.bind(this, context, 'secondNumber')} type="number"/>
                    </div>
                </div>
            )}
        </NumberContextConsumer>
    )
}


export default NumberSelector;
