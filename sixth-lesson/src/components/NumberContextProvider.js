import React, {Component} from 'react';
const {Provider, Consumer} = React.createContext();

class NumberContextProvider extends Component {
    state = {
        firstNumber: 0,
        secondNumber: 0,
        sign: '+',
    }

    onNumberChange = (number, value) => {
        this.setState({[number]: value})
    }

    onSignChange = event => this.setState({sign: event.target.value});

    render() {
        return (
            <Provider value={{...this.state, onNumberChange: this.onNumberChange, onSignChange: this.onSignChange}}>
                {this.props.children}
            </Provider>
        )
    }
}

export {NumberContextProvider, Consumer as NumberContextConsumer}
