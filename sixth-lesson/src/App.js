import React from 'react';
import './App.css';
import NumberSelector from './components/NumberSelector';
import NumberCalculator from './components/NumberCalculator';

function App() {
    return (
        <React.Fragment>
            <NumberSelector/>
            <NumberCalculator/>
        </React.Fragment>
    );
}

export default App;
