const setNodesProxy = obj => {
    return new Proxy(obj, {
        get(target, key) {
            if (key === 'toJSON') {
                return Reflect.get(target, key);
            }

            const value = Reflect.get(target, key);

            if (value === undefined) {
                Reflect.set(target, key, setNodesProxy({}));
                return Reflect.get(target, key);
            }

            return value
        },
        set(target, key, value) {
            if (typeof value === 'object') {
                Reflect.set(target, key, setNodesProxy(value));
            } else {
                Reflect.set(target, key, value);
            }
        }
    })
}

const sampleObject = {x: 10};

const proxied = setNodesProxy(sampleObject);

proxied.a = {};
proxied.a.b.c = 1;
proxied.b.c.d = 4;
proxied.b.c.e = 5;

console.log(JSON.stringify(proxied));
