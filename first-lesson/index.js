const delayedStart = (delay) => {
    let newDelay = 0;
    const delayedFunctions = [];

    const delayedRun = (delay, func, ...args) => {
        return new Promise(resolve => setTimeout(() => resolve(func(...args)), delay));
    }

    return func => async (...args) => {
        if (!newDelay) {
            newDelay = delay;
        }

        delayedFunctions.push(delayedRun(newDelay, func, ...args))
        newDelay += delay;

        return await Promise.all(delayedFunctions);
    }
}

const sampleFunc = value => `Sample: ${value}`;

const delayedHandler = delayedStart(2000);

delayedHandler(sampleFunc)('asf1').then(console.log);
delayedHandler(sampleFunc)('asf2').then(console.log)
